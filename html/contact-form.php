﻿<?php
$to="demo@company.com";/*Your Email*/

$subject="Message from the website";

$date=date("l, F jS, Y");
$time=date("h:i A");
$firstName=$_REQUEST['name'];
$email=$_REQUEST['email'];
$phone=$_REQUEST['phone'];

$msg="
	Message sent from website on date:  $date, hour: $time.\n
	Name: $firstName\n
	Phone: $phone\n
	Email: $email
	";
if($email=="" or $firstName=="" or $phone=="") {
echo "<div class='alert alert-danger'>
		  <a class='close' data-dismiss='alert'>×</a>
		  <strong>Warning!</strong> Please fill all the fields.
	  </div>";
} else {
mail($to,$subject,$msg,"From:".$email);
echo "<div class='alert alert-success'>
		  <a class='close' data-dismiss='alert'>×</a>
		  <strong>Thank you for your message!</strong>
	  </div>";
}
?>
